var angular = require('angular');

var service = require('./app/services/AppService');
var App = require('./app/containers/App');
var Login = require('./app/components/Login');
var Header = require('./app/components/Header');
var ChatSection = require('./app/components/ChatSection');
var MessageTextInput = require('./app/components/MessageTextInput');
var MessageItem = require('./app/components/MessageItem');
require('angular-ui-router');
var routesConfig = require('./routes');

import './index.css';

angular
  .module('app', ['ui.router'])
  .config(routesConfig)
  .service('appService', service.AppService)
  .component('app', App)
  .component('login', Login)
  .component('headerComponent', Header)
  .component('chatSection', ChatSection)
  .component('messageTextInput', MessageTextInput)
  .component('messageItem', MessageItem);
