require('es6-shim');

function AppService($http, $interval, $window) {
  var self = this;
  this.botMessages = [];
  this.$interval = $interval;
  this.$window = $window;
  this.audio = new Audio('assets/sound.mp3');

  $http({
    method: 'GET',
    url: 'assets/Messages.json'
  }).then(function (success) {
    self.botMessages = angular.fromJson(success.data);
  });
}

AppService.prototype = {
  getLogin: function () {
    return this.$window.localStorage.login;
  },

  setLogin: function (login) {
    this.$window.localStorage.setItem('login', login);
    return true;
  },

  addMessage: function (text, messages) {
    this.audio.play();
    return [
      {
        id: (messages.length === 0) ? 0 : messages[0].id + 1,
        author: this.getLogin(),
        own: true,
        date: new Date(),
        text: text
      }
    ].concat(messages);
  },

  addBotMessage: function (messages) {
    this.message = this.botMessages[Math.floor((Math.random() * this.botMessages.length))];
    var self = this;
    this.$interval(function () {
      self.message = self.botMessages[Math.floor((Math.random() * self.botMessages.length))];
    }, 5000);

    return [
      {
        id: (messages.length === 0) ? 0 : messages[0].id + 1,
        author: this.message.author,
        own: false,
        date: new Date(),
        text: this.message.text
      }
    ].concat(messages);
  }

};

module.exports = {
  AppService: AppService
};

