module.exports = {
  template: require('./Header.html'),
  controller: Header,
  bindings: {
    messages: '='
  }
};

/** @ngInject */
function Header(appService, $interval, $state) {
  this.appService = appService;
  this.$state = $state;
  this.$interval = $interval;
  var self = this;

  if (!this.appService.getLogin()) {
    this.$state.go('login');
  }

  this.$interval(function () {
    self.messages = self.appService.addBotMessage(self.messages);
  }, 5000);
}

Header.prototype = {
  handleSave: function (text) {
    if (text.length !== 0) {
      this.messages = this.appService.addMessage(text, this.messages);
    }
  },
  logout: function () {
    this.appService.setLogin('');
    this.$state.go('login');
  }
};
