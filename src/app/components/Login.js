module.exports = {
  template: require('./Login.html'),
  controller: Login
};

/** @ngInject */
function Login(appService, $window, $timeout, $state) {
  this.$timeout = $timeout;
  this.$window = $window;
  this.$state = $state;
  this.appService = appService;
  this.login = '';
}

Login.prototype = {

  submit: function (e) {
    if (e.keyCode === 13 && this.login !== '') {
      this.appService.setLogin(this.login);
      this.$state.go('app');
    }
  }
};
