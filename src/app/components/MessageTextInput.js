module.exports = {
  template: require('./MessageTextInput.html'),
  controller: MessageTextInput,
  bindings: {
    onSave: '&',
    placeholder: '@',
    newMessage: '@',
    editing: '@',
    text: '<'
  }
};

/** @ngInject */
function MessageTextInput(appService, $window, $timeout) {
  this.$timeout = $timeout;
  this.$window = $window;
  this.appService = appService;
  this.editing = this.editing || false;
  this.text = this.text || '';
  if (this.text.length) {
    this.focus();
  }
}

MessageTextInput.prototype = {
  handleBlur: function () {
    if (!this.newMessage) {
      this.onSave({text: this.text});
    }
  },

  handleSubmit: function (e) {
    if (e.keyCode === 13) {
      this.onSave({text: this.text});
      if (this.newMessage) {
        this.text = '';
      }
    }
  },

  focus: function () {
    this.$timeout(function () {
      var element = this.$window.document.querySelector('.editing .textInput');
      if (element) {
        element.focus();
      }
    }, 0);
  }
};
