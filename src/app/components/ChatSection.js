
module.exports = {
  template: require('./ChatSection.html'),
  controller: ChatSection,
  bindings: {
    messages: '='
  }
};

/** @ngInject */
function ChatSection(appService) {
  this.appService = appService;
}

ChatSection.prototype = {
};
